#!/bin/bash

while true
do
	echo -e "Selecione uma opção:\nExecutar ping para site (1)\nListar usuários logados (2)\nExibir uso de memória e disco (3)\nSair (4)\n"
	read num

	if [ $num -eq 1 ]
	then
		echo "Digite um ip:"
		read ip
		ping $ip
	fi
	if [ $num -eq 2 ]
	then
		whoami
	fi
	if [ $num -eq 3 ]
	then
		df -h
	fi
	if [ $num -eq 4 ]
	then
		exit 0
	fi
done
